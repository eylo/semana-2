package ucenfotec.heliomar;

import java.io.*;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void main(String[] args) throws java.io.IOException {
        int opcion = -1;
        do {
            opcion = mostrarMenu();
            procesarOpcionMenu(opcion);
        } while (opcion != 0);
    }

         static int mostrarMenu() throws java.io.IOException {
             int opcion = 0;
             out.println("1. Registrar marcas");
             out.println("2. Imprimir las marcas");
             out.println("0. Salir del programa");
             opcion = Integer.parseInt(in.readLine());
             return opcion;
         }
    static void procesarOpcionMenu(int pOpcion) throws IOException {
        switch (pOpcion){
            case 1:
                solicitarInfoRegistroMarca();
                break;
            case 2:
                imprimirMarcas();
                break;
            default:
                break;
        }
    }

    static void solicitarInfoRegistroMarca() throws java.io.IOException{
        int cantMarcas = 5;
        for(int i = 0; i < cantMarcas; i++) {

            out.println("Por favor ingrese el nombre de la marca de automóvil a registrar");
            String marca = in.readLine();
            CL.registrarMarca(marca, i);
        }
    }

    static void imprimirMarcas(){
        String[] arregloMarcas = CL.obtenerArregloMarcas();
        for(int i = 0; i < arregloMarcas.length; i++) {
            out.println(arregloMarcas[i]);
        }
    }

}
