package ucenfotec.heliomar;

public class Carrera {

    String codigo;
    String nombre;
    Boolean esAcreditada;

    public Carrera (String pcodigo, String pnombre, Boolean pesAcreditada){

        this.codigo = pcodigo;
        this.nombre = pnombre;
        this.esAcreditada = pesAcreditada;

    }

    public void modificarNombre(String pnombre){
        this.nombre = pnombre;
    }
    public String obtenerNombre(){
        return nombre;
    }

}
